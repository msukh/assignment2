class CreateCommentals < ActiveRecord::Migration
  def change
    create_table :commentals do |t|
      t.integer :post_id
      t.text :body

      t.timestamps
    end
  end
end
