class CreatePostals < ActiveRecord::Migration
  def change
    create_table :postals do |t|
      t.string :title
      t.text :body

      t.timestamps
    end
  end
end
