class PostalsController < ApplicationController
  before_action :set_postal, only: [:show, :edit, :update, :destroy]

  # GET /postals
  # GET /postals.json
  def index
    @postals = Postal.all
  end

  # GET /postals/1
  # GET /postals/1.json
  def show
  end

  # GET /postals/new
  def new
    @postal = Postal.new
  end

  # GET /postals/1/edit
  def edit
  end

  # POST /postals
  # POST /postals.json
  def create
    @postal = Postal.new(postal_params)

    respond_to do |format|
      if @postal.save
        format.html { redirect_to @postal, notice: 'Postal was successfully created.' }
        format.json { render :show, status: :created, location: @postal }
      else
        format.html { render :new }
        format.json { render json: @postal.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /postals/1
  # PATCH/PUT /postals/1.json
  def update
    respond_to do |format|
      if @postal.update(postal_params)
        format.html { redirect_to @postal, notice: 'Postal was successfully updated.' }
        format.json { render :show, status: :ok, location: @postal }
      else
        format.html { render :edit }
        format.json { render json: @postal.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /postals/1
  # DELETE /postals/1.json
  def destroy
    @postal.destroy
    respond_to do |format|
      format.html { redirect_to postals_url, notice: 'Postal was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_postal
      @postal = Postal.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def postal_params
      params.require(:postal).permit(:title, :body)
    end
end
