class CommentalsController < ApplicationController
  before_action :set_commental, only: [:show, :edit, :update, :destroy]

  # GET /commentals
  # GET /commentals.json
  def index
    @commentals = Commental.all
  end

  # GET /commentals/1
  # GET /commentals/1.json
  def show
  end

  # GET /commentals/new
  def new
    @commental = Commental.new
  end

  # GET /commentals/1/edit
  def edit
  end

  # POST /commentals
  # POST /commentals.json
  def create
    @commental = Commental.new(commental_params)

    respond_to do |format|
      if @commental.save
        format.html { redirect_to @commental, notice: 'Commental was successfully created.' }
        format.json { render :show, status: :created, location: @commental }
      else
        format.html { render :new }
        format.json { render json: @commental.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /commentals/1
  # PATCH/PUT /commentals/1.json
  def update
    respond_to do |format|
      if @commental.update(commental_params)
        format.html { redirect_to @commental, notice: 'Commental was successfully updated.' }
        format.json { render :show, status: :ok, location: @commental }
      else
        format.html { render :edit }
        format.json { render json: @commental.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /commentals/1
  # DELETE /commentals/1.json
  def destroy
    @commental.destroy
    respond_to do |format|
      format.html { redirect_to commentals_url, notice: 'Commental was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_commental
      @commental = Commental.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def commental_params
      params.require(:commental).permit(:post_id, :body)
    end
end
