json.array!(@commentals) do |commental|
  json.extract! commental, :id, :post_id, :body
  json.url commental_url(commental, format: :json)
end
