json.array!(@postals) do |postal|
  json.extract! postal, :id, :title, :body
  json.url postal_url(postal, format: :json)
end
