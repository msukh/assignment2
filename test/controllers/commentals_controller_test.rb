require 'test_helper'

class CommentalsControllerTest < ActionController::TestCase
  setup do
    @commental = commentals(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:commentals)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create commental" do
    assert_difference('Commental.count') do
      post :create, commental: { body: @commental.body, post_id: @commental.post_id }
    end

    assert_redirected_to commental_path(assigns(:commental))
  end

  test "should show commental" do
    get :show, id: @commental
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @commental
    assert_response :success
  end

  test "should update commental" do
    patch :update, id: @commental, commental: { body: @commental.body, post_id: @commental.post_id }
    assert_redirected_to commental_path(assigns(:commental))
  end

  test "should destroy commental" do
    assert_difference('Commental.count', -1) do
      delete :destroy, id: @commental
    end

    assert_redirected_to commentals_path
  end
end
